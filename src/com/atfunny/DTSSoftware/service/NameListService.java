package com.atfunny.DTSSoftware.service;

import com.atfunny.DTSSoftware.domain.*;

import static com.atfunny.DTSSoftware.service.Data.*;   //静态导入Data包

/**
 * @ClassName NameListService
 * @Description 负责将Data中的数据封装到Employee[]数组中，同时提供相关操作Employee[]的方法。
 * @Author AqFunny
 * @Date 2021/9/14 15:33
 * @Version 1.0
 */
public class NameListService {
    private Employee[] employees;   //保存公司所有的员工对象

    /**
    * @Description 利用构造器，导入封装类 Data 重的员工数据，对 employees 数组进行初始化
    * @Date 16:31 2021/9/14
    * @Param []
    * @return
    **/
    public NameListService() {
        employees = new Employee[EMPLOYEES.length]; //开辟公司员工个数大小的空间

        //通过循环逐个创建不同工种的员工对象
        for (int i = 0; i < EMPLOYEES.length; i++) {

            //获取每一个职位都有的属性
            int type = Integer.parseInt(EMPLOYEES[i][0]);   //工种
            int id = Integer.parseInt(EMPLOYEES[i][1]);     //工号
            String name = EMPLOYEES[i][2];                  //姓名
            int age = Integer.parseInt(EMPLOYEES[i][3]);    //年龄
            double salary = Double.parseDouble(EMPLOYEES[i][4]);    //工资

            //特殊工种特定的属性获取
            int stock;              //声明股票数量
            double bonus;           //声明奖金
            Equipment equipment;    //声明设备变量

            //通过不同的工种实例化不同的对象
            switch(type) {
                case EMPLOYEE: {
                    employees[i] = new Employee(id, name, age, salary);
                    break;
                }
                case PROGRAMMER: {
                    equipment = creatEquipment(i);  //programmer 的设备
                    employees[i] = new Programmer(id, name, age, salary, equipment);
                    break;
                }
                case DESIGNER: {
                    bonus = Double.parseDouble(EMPLOYEES[i][5]);    //designer 的奖金
                    equipment = creatEquipment(i);                  //designer 的设备
                    employees[i] = new Designer(id, name, age, salary, equipment, bonus);
                    break;
                }
                case ARCHITECT: {
                    bonus = Double.parseDouble(EMPLOYEES[i][5]);    //architect 的奖金
                    equipment = creatEquipment(i);                  //architect 的设备
                    stock = Integer.parseInt(EMPLOYEES[i][6]);      //architect 的股票数量
                    employees[i] = new Architect(id, name, age, salary, equipment, bonus, stock);
                    break;
                }
            }
        }
    }
    /**
    * @Description 获取所有员工信息
    * @Date 10:55 2021/9/16
    * @Param []
    * @return com.atfunny.domain.Employee[]
    **/
    public Employee[] getAllEmployees() {
        return employees;
    }
    /**
    * @Description 通过 id 索引返回员工信息
    * @Date 10:56 2021/9/16
    * @Param [id]
    * @return com.atfunny.domain.Employee
    **/
    public Employee getEmployee (int id) throws TeamException{
        for(int i = 0; i < employees.length; i++) {
            if(employees[i].getId() == id) {
                return employees[i];
            }
        }
        throw new TeamException("该员工不存在！"); //抛出异常
    }
    /**
    * @Description 根据不同的设备号创建不同的设备对象
    * @Date 16:30 2021/9/14
    * @Param [index]
    * @return com.atfunny.domain.Equipment
    **/
    public Equipment creatEquipment(int index) {
        int type = Integer.parseInt(EQUIPMENTS[index][0]);

        //分支根据设备号创建对应的设备对象
        switch(type) {
            case PC: {
                return new PC(EQUIPMENTS[index][1], EQUIPMENTS[index][2]);
            }
            case NOTEBOOK: {
                return new NoteBook(EQUIPMENTS[index][1], Double.parseDouble(EQUIPMENTS[index][2]));
            }
            case PRINTER: {
                return new Printer(EQUIPMENTS[index][1], EQUIPMENTS[index][2]);
            }
        }
        return null;
    }
}
