package com.atfunny.DTSSoftware.service;

/**
 * @ClassName TeamException
 * @Description 自定义团队操作异常
 * @Author AqFunny
 * @Date 2021/9/14 14:02
 * @Version 1.0
 */
public class TeamException extends Exception {
    static final long serialVersionUID = -7034897115645766939L;

    public TeamException() {

    }

    public TeamException(String message) {
        super(message);
    }
}
