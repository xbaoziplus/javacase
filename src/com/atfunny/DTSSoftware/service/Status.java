package com.atfunny.DTSSoftware.service;

/**
 * @ClassName Status
 * @Description 封装员工的状态
 * @Author AqFunny
 * @Date 2021/9/14 14:39
 * @Version 1.0
 */
public class Status {
    private final String STATUS;      //记录用户状态

    private Status(String STATUS) {
        this.STATUS = STATUS;
    }
    public static final Status FREE = new Status("FREE");
    public static final Status BUSY = new Status("BUSY");
    public static final Status VOCATION = new Status("VOCATION");

    public String getSTATUS() {
        return STATUS;
    }

    @Override
    public String toString() {
        return STATUS;
    }
}
