package com.atfunny.DTSSoftware.service;

import com.atfunny.DTSSoftware.domain.Architect;
import com.atfunny.DTSSoftware.domain.Designer;
import com.atfunny.DTSSoftware.domain.Employee;
import com.atfunny.DTSSoftware.domain.Programmer;
import com.atfunny.DTSSoftware.view.TSUtility;

/**
 * @ClassName TeamService
 * @Description 用于开发团队成员的管理，如增加、删除等
 * @Author AqFunny
 * @Date 2021/9/14 16:53
 * @Version 1.0
 */
public class TeamService {
    private int total = 0;              //团队人员总数
    private int numOfArchitect = 0;     //团队架构师人数
    private int numOfDesigner = 0;      //团队设计师人数
    private int numOfProgrammer = 0;    //团队程序员人数
    private static int counter = 1;     //自动生成团队成员的 memberId
    private final int MAX_MEMBER = 5;   //团队最大容纳量
    private Programmer[] team = new Programmer[MAX_MEMBER]; //开辟一个可容纳最大成员的数组空间

    /**
    * @Description 返回开发团队成员
    * @Date 16:59 2021/9/14
    * @Param []
    * @return com.atfunny.domain.Programmer[]
    **/
    public Programmer[] getTeam() {
        Programmer[] realTeam = new Programmer[total];
        for (int i = 0; i < total; i++) {
            realTeam[i] = team[i];
        }
        return realTeam;
    }

    /**
    * @Description 添加团队成员，若该成员不存在，则抛出 TeamException 异常
    * @Date 16:59 2021/9/14
    * @Param [e]
    * @return void
    **/
    public void addMember(Employee e) throws TeamException {
        //排除成员已满以及成员自身原因无法加入的情况
        if(total >= MAX_MEMBER) {
            throw new TeamException("团队成员已满，无法继续添加");
        } else if(!(e instanceof Programmer)) {
            throw new TeamException("员工 " + e.getName() + " 不是开发人家，添加失败");
        }

        Programmer p = (Programmer) e;  //传参体现多态，需要强转

        //判断成员现状是否符合加入团队
        if(isExist(p)) {
            throw new TeamException("员工 " + e.getName() + " 已加入本团队");
        } else if(p.getStatus().equals("VOCATION")) {
            throw new TeamException("员工 " + e.getName() + " 正在休假中，无法进行添加");
        } else if(p.getStatus().equals("BUSY")) {
            throw new TeamException("员工 " + e.getName() + " 已加入了某团队");
        }

        //添加确定环节，避免添加错误
        System.out.print("是否确认将 " + e.getName() + " 加入团队(Y/N)：");
        if(TSUtility.readConfirmSelection() == 'N') {
            throw new TeamException("取消添加");
        }

        //排除团队某个职位已满无法加入的情况
        if(p instanceof Architect) {
            if(numOfArchitect >= 1) {
                throw new TeamException("团队中至多拥有一名架构师");
            }
            numOfArchitect++;
        } else if(p instanceof Designer) {
            if(numOfDesigner >= 2) {
                throw new TeamException("团队中至多拥有两名设计师");
            }
            numOfDesigner++;
        } else {
            if(numOfProgrammer >= 3) {
                throw new TeamException("团队中至多拥有三名程序员");
            }
            numOfProgrammer++;
        }

        //将员工加入团队
        p.setStatus(Status.BUSY);   //职员进入团队开发后状态变成BUSY
        p.setMemberId(counter++);   //职员进入团队后在团队中的 Id 号
        team[total++] = p;          //将职员 p 插入到团队中并团队人数加一
    }

    /**
    * @Description 判断该成员是否已存在于团队中
    * @Date 20:40 2021/9/14
    * @Param [p]
    * @return boolean
    **/
    public boolean isExist(Programmer p) {
        for (int i = 0; i < total; i++) {
            if(team[i].getId() == p.getId()) {
                return true;
            }
        }
        return false;
    }
    /**
    * @Description 通过在团队中的团队 Id 删除团队成员，若该成员不存在，则抛出 TeamException 异常
    * @Date 20:36 2021/9/14
    * @Param [memberId]
    * @return void
    **/
    public void removeMember(int memberId) throws TeamException{
        //团队为空时，抛出异常
        if(total == 0) {
            throw new TeamException("团队中没有成员，无法删除");
        }

        //遍历团队查找目标成员
        int n = 0;
        if(memberId > counter) {        //当输入团队成员 Id 大于记录的最大 Id 时证明不存在该成员，抛出异常
            throw new TeamException("找不到该成员，无法删除");
        } else {                        //当输入团队成员 Id 小于等于最大 Id 时遍历搜索该成员
            while(n < total) {
                if(team[n++].getMemberId() == memberId) {
                    System.out.print("是否确认删除团队成员 " + team[--n].getName() + "(Y/N): ");
                    char isDelete = TSUtility.readConfirmSelection();
                    if(isDelete == 'N') {
                        throw new TeamException("取消删除");
                    }
                    team[n].setStatus(Status.FREE);//将目标职员的状态改成 FREE
                    break;
                }
            }
            if(n == total) {        //如果遍历搜索失败，则证明该成员不存在，抛出异常
                throw new TeamException("找不到该成员，无法删除");
            }
        }
        if(team[n] instanceof Architect) {
            numOfArchitect--;
        } else if(team[n] instanceof Designer) {
            numOfDesigner--;
        } else {
            numOfProgrammer--;
        }
        System.out.println("成员" + team[n].getName() + "已被移除团队");

        //移动目标成员之后的成员，补上 team 数组中的空缺
        for (int i = n + 1; i < total; i++) {
            team[i - 1] = team[i];
        }
        team[--total] = null;       //将最后一个没人的位置置空，同时团队人数减一
    }

    public int getTotal() {
        return total;
    }

    public int getNumOfArchitect() {
        return numOfArchitect;
    }

    public int getNumOfDesigner() {
        return numOfDesigner;
    }

    public int getNumOfProgrammer() {
        return numOfProgrammer;
    }

    public static int getCounter() {
        return counter;
    }

    public int getMAX_MEMBER() {
        return MAX_MEMBER;
    }
}
