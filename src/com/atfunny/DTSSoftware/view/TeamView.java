package com.atfunny.DTSSoftware.view;

import com.atfunny.DTSSoftware.domain.*;
import com.atfunny.DTSSoftware.service.NameListService;
import com.atfunny.DTSSoftware.service.TeamException;
import com.atfunny.DTSSoftware.service.TeamService;

/**
 * @ClassName TeamView
 * @Description 在本类中通过与用户的交互，逐一实现各个功能
 * @Author AqFunny
 * @Date 2021/9/15 17:25
 * @Version 1.0
 */
public class TeamView {
    private NameListService listService = new NameListService();    //公司员工
    private TeamService teamService = new TeamService();            //团队成员

    public static void main(String[] args) {
        new TeamView().enterMainMenu();
    }
    /**
    * @Description 主菜单界面
    * @Date 17:33 2021/9/15
    * @Param []
    * @return void
    **/
    public void enterMainMenu() {
        boolean isLoop = true;
        int selection = 0;
//        TeamView menu = new TeamView();   //如果写成这样子的话就会出错，公司名单无法更新
        while(isLoop) {
            if(selection != 1) {
                listAllEmployees();         //错误：menu.listAllEmployees();
            }
            System.out.print("1-团队列表  2-添加团队成员  3-删除团队成员 4-退出   请选择(1-4)：");
            selection = TSUtility.readMenuSelection();
            switch(selection) {
                case 1: {
                    listTeam();     //团队列表
                    break;
                }
                case 2: {
                    addMember();    //添加成员
                    break;
                }
                case 3: {
                    deleteMember(); //删除成员
                    break;
                }
                case 4: {
                    System.out.print("确认是否退出(Y/N)：");
                    char confirm = TSUtility.readConfirmSelection();
                    if(confirm == 'Y') {
                        isLoop = false;
                    }
                    break;
                }
            }
        }
    }
    /**
    * @Description 以表格形式列出公司所有成员
    * @Date 17:34 2021/9/15
    * @Param []
    * @return void
    **/
    private void listAllEmployees() {
        System.out.println("\n-------------------------------开发团队调度软件--------------------------------\n");
        Employee[] employees = listService.getAllEmployees();
        if(employees.length == 0) {
            System.out.println("没有客户信息");
        } else {
            System.out.println("ID\t\t姓名\t\t年龄\t\t工资\t\t\t职位\t\t状态\t\t奖金\t\t\t股票\t\t领用设备");
        }
        for (int i = 0; i < employees.length; i++) {
            System.out.println(employees[i]);
        }
    }
    /**
    * @Description 显示团队成员列表
    * @Date 17:34 2021/9/15
    * @Param []
    * @return void
    **/
    private void listTeam() {
        System.out.println("\n--------------------团队成员列表---------------------\n");
        Programmer[] team = teamService.getTeam();
        if(team.length == 0) {
            System.out.println("开发团队中没有检测到成员信息！");
        } else {
            System.out.println("TID/ID\t姓名\t\t年龄\t\t工资\t\t\t职位\t\t状态\t\t奖金\t\t\t股票\t\t领用设备");
        }
        for (int i = 0; i < team.length; i++) {
            System.out.println(team[i].getMemberId() + "/" + team[i]);
        }
        System.out.println("\n-----------------------------------------------------");
    }
    /**
    * @Description 实现添加团队成员操作
    * @Date 17:34 2021/9/15
    * @Param []
    * @return void
    **/
    private void addMember() {
        System.out.println("---------------------添加成员---------------------");
        System.out.print("请输入需要添加的员工编号：");
        int id = TSUtility.readInt();
        try {
            Employee employee = listService.getEmployee(id);
            teamService.addMember(employee);
            System.out.println("员工" + employee.getName() + "成功加入团队");
        } catch (TeamException e) {
            System.out.println("添加失败！原因：" + e.getMessage());
        }
        TSUtility.readReturn();
    }
    /**
    * @Description 实现删除团队成员操作
    * @Date 17:35 2021/9/15
    * @Param []
    * @return void
    **/
    private void deleteMember() {
        System.out.println("---------------------删除成员---------------------");
        System.out.print("请输入需要删除的成员团队编号：");
        int id = TSUtility.readInt();
        try {
            teamService.removeMember(id);
        } catch (TeamException e) {
            System.out.println("删除失败！原因：" + e.getMessage());
        }
        TSUtility.readReturn();
    }
}
