package com.atfunny.DTSSoftware.view;

import java.util.Scanner;

/**
 * @ClassName TSUtility
 * @Description 可用来方便地实现键盘访问。
 * @Author AqFunny
 * @Date 2021/9/14 12:03
 * @Version 1.0
 */
public class TSUtility {
    private static Scanner sc = new Scanner(System.in);

    /**
     * @return char
     * @Description 该方法读取键盘，如果用户键入合法选项中的任意选项，则方法返回。返回值为用户键入选项。
     * @Date 12:07 2021/9/14
     * @Param []
     **/
    public static int readMenuSelection() {
        int selection;
        while (true) {
            selection = Integer.parseInt(readKeyBoard(1, false));
            if (selection < 1 || selection > 4) {
                System.out.print("选项不存在！请重新输入：");
                continue;
            }
            break;
        }
        return selection;
    }

    /**
     * @return void
     * @Description 该方法提示并等待，直到用户按回车键后返回。
     * @Date 12:09 2021/9/14
     * @Param []
     **/
    public static void readReturn() {
        System.out.println("按回车键继续……");
        readKeyBoard(100, true);    //传递true，通过返回一个空字符实现这一功能
    }

    /**
     * @return int
     * @Description 该方法从键盘读取一个长度不超过2位的整数，并将其作为方法的返回值。
     * @Date 12:09 2021/9/14
     * @Param []
     **/
    public static int readInt() {
        int number;
        while (true) {
            try {
                number = Integer.parseInt(readKeyBoard(2, false));
                break;
            } catch (NumberFormatException e) {
                System.out.print("数字输入错误，请重新输入：");
            }
        }
        return number;
    }

    /**
     * @return char
     * @Description 从键盘读取‘Y’或’N’，并将其作为方法的返回值。
     * @Date 12:09 2021/9/14
     * @Param []
     **/
    public static char readConfirmSelection() {
        char confirm;
        while (true) {
            confirm = readKeyBoard(1, false).toUpperCase().charAt(0);
            if (confirm != 'Y' && confirm != 'N') {
                System.out.print("选项 " + confirm + " 错误，请重新输入：");
                continue;
            }
            break;
        }
        return confirm;
    }

    /**
     * @return java.lang.String
     * @Description 从键盘读取限长 limit 的字符串并只限于本类中调用，返回值为该字符串
     * @Date 12:09 2021/9/14
     * @Param [limit, blankReturn] 若blankReturn为 true，则可返回空串，否则必须输入非空串
     **/
    private static String readKeyBoard(int limit, boolean blankReturn) {
        String line = "";
        while (sc.hasNextLine()) {
            line = sc.nextLine();
            if (line.length() == 0) {
                if (blankReturn) {
                    return line;
                } else {
                    continue;
                }
            }
            if (line.length() < 1 || line.length() > limit) {
                System.out.print("长度应不大于 " + limit + ", 请重新输入：");
                continue;
            }
            break;
        }
        return line;
    }
}
