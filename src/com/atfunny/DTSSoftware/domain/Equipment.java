package com.atfunny.DTSSoftware.domain;

/**
 * @InterfaceName Equipment
 * @Description 员工设备接口
 * @Author AqFunny
 * @Date 2021/9/14 14:07
 * @Version 1.0
 */
public interface Equipment {
    /**
    * @Description 获取设备信息
    * @Date 14:08 2021/9/14
    * @Param []
    * @return java.lang.String
    **/
    String getDescription();
}
