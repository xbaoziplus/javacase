package com.atfunny.DTSSoftware.domain;

import com.atfunny.DTSSoftware.service.Status;

import static com.atfunny.DTSSoftware.service.Status.*; //静态导入，避免下方 FREE 繁琐的前缀

/**
 * @ClassName Programmer
 * @Description 程序员封装类
 * @Author AqFunny
 * @Date 2021/9/14 14:38
 * @Version 1.0
 */
public class Programmer extends Employee {
    private int memberId;           //成员加入开发团队后在团队中的ID
    private Status status = FREE;   //上方静态导入了 Status 包，因此可以直接调用FREE
    private Equipment equipment;

    public Programmer() {

    }

    public Programmer(int id, String name, int age, double salary, Equipment equipment) {
        super(id, name, age, salary);
        this.equipment = equipment;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    @Override
    public String toString() {
        return getDetail() + "\t程序员\t" + getStatus() + "\t\t\t\t\t\t" + getEquipment().getDescription();
    }
}
