package com.atfunny.DTSSoftware.domain;

/**
 * @ClassName NoteBook
 * @Description NoteBook 设备
 * @Author AqFunny
 * @Date 2021/9/14 14:15
 * @Version 1.0
 */
public class NoteBook implements Equipment{
    private String model;       //机器型号
    private double price;       //机器价格

    public NoteBook() {

    }

    public NoteBook(String model, double price) {
        this.model = model;
        this.price = price;
    }

    @Override
    public String getDescription() {
        return model + "(" + price + ")";
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
