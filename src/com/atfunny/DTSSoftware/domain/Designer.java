package com.atfunny.DTSSoftware.domain;

/**
 * @ClassName Designer
 * @Description 设计师封装类
 * @Author AqFunny
 * @Date 2021/9/14 14:54
 * @Version 1.0
 */
public class Designer extends Programmer{
    private double bonus;   //奖金

    public Designer() {

    }

    public Designer(int id, String name, int age, double salary, Equipment equipment, double bonus) {
        super(id, name, age, salary, equipment);
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return getDetail() + "\t设计师\t" + getStatus() + "\t" + bonus + "\t\t\t\t" + getEquipment().getDescription();
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}
