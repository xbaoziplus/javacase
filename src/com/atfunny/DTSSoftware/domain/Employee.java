package com.atfunny.DTSSoftware.domain;

/**
 * @InterfaceName Employee
 * @Description 普通职员封装类
 * @Author AqFunny
 * @Date 2021/9/14 14:33
 * @Version 1.0
 */
public class Employee {
    private int id;         //员工工号
    private String name;    //员工姓名
    private int age;        //员工年龄
    private double salary;  //员工工资

    public Employee() {

    }

    public Employee(int id, String name, int age, double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDetail() {
        return id + "\t\t" + name + "\t" + age + "\t\t" + salary + "\t";
    }

    @Override
    public String toString() {
        return getDetail();
    }
}
