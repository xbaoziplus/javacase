package com.atfunny.DTSSoftware.domain;

/**
 * @ClassName Architect
 * @Description 架构师封装类
 * @Author AqFunny
 * @Date 2021/9/14 15:08
 * @Version 1.0
 */
public class Architect extends Designer {
    private int stock;  //公司奖励的股票数量

    public Architect() {

    }

    public Architect(int id, String name, int age, double salary, Equipment equipment, double bonus, int stock) {
        super(id, name, age, salary, equipment, bonus);
        this.stock = stock;
    }

    @Override
    public String toString() {
        return getDetail() + "\t架构师\t" + getStatus() + "\t" + getBonus() + "\t\t" + stock + "\t" + getEquipment().getDescription();
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
