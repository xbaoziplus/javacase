package com.atfunny.BillManagementSystem.ui;

import com.atfunny.BillManagementSystem.javabean.Customer;
import com.atfunny.BillManagementSystem.service.CustomerList;
import com.atfunny.BillManagementSystem.service.CustomerUtility;

/**
 * @ClassName CustomerView
 * @Description 为主模块，负责菜单的显示和处理用户操作
 * @Author AqFunny
 * @Date 2021/8/26 10:38
 * @Version 1.0
 */
public class CustomerView {
    public static void main(String[] args) {
        CustomerView customerView = new CustomerView();     //实例化对象
        customerView.enterMainMenu();                       //调用主菜单函数
    }
    private CustomerList customerList = new CustomerList(10);
    /**
    * @Description 使用无参构造器对程序进行初始化，在程序中增加默认值和案例
    * @Date 17:38 2021/8/30
    * @Param []
    * @return
    **/
    public CustomerView() {
        Customer customer = new Customer("张三", '男', 30, "010-56253825", "abc@email.com");
        customerList.addCustomer(customer);
    }
    /**
    * @Description 用于显示主菜单，响应用户输入，根据用户操作分别调用其他相应的成员方法，从而完成客户信息处理
    * @Date 21:08 2021/8/29
    * @Param []
    * @return void
    **/
    public void enterMainMenu() {
        boolean loopFlag = true;
        do {
            System.out.println("\n-----------------客户信息管理系统-----------------\n");
            System.out.println("\t\t1. 添 加 客 户\t\t\t2. 修 改 客 户");
            System.out.println("\t\t3. 删 除 客 户\t\t\t4. 客 户 列 表");
            System.out.println("\t\t5. 退       出");
            System.out.print("\t\t请选择(1-5)：");
            int selection = CustomerUtility.readMenuSelection(5);
            switch(selection) {
                case 1: {
                    addNewCustomer();   //添加客户信息
                    break;
                }
                case 2: {
                    modifyCustomer();   //修改客户信息
                    break;
                }
                case 3: {
                    deleteCustomer();   //删除客户信息
                    break;
                }
                case 4: {
                    listAllCustomers(); //遍历客户列表
                    break;
                }
                case 5: {
                    System.out.print("确认是否退出(Y / N): ");
                    if(CustomerUtility.readConfirmSelection() == 'Y') {
                        loopFlag = false;
                    }
                    break;
                }
            }
        }while(loopFlag);
    }
    /**
    * @Description 用于添加新用户，且只允许 enterMainMenu 方法调用
    * @Date 21:12 2021/8/29
    * @Param []
    * @return void
    **/
    private void addNewCustomer() {
        Customer customer = new Customer();
        System.out.println("---------------------添加客户---------------------");
        System.out.print("姓名：");
        customer.setName(CustomerUtility.readString(4));
        System.out.print("性别：");
        customer.setGender(CustomerUtility.readChar());
        System.out.print("年龄：");
        customer.setAge(CustomerUtility.readInt());
        System.out.print("电话：");
        customer.setPhone(CustomerUtility.readString(15));
        System.out.print("邮箱：");
        customer.setEmail(CustomerUtility.readString(20));

        if(customerList.addCustomer(customer)) {
            System.out.println("---------------------添加完成---------------------");
        } else {
            System.out.println("----------------记录已满,无法添加-----------------");
        }
    }
    /**
    * @Description 实例化一个新的客户对象传递给 replaceCustomer 方法从而达到修改客户信息的作用
    * @Date 12:43 2021/8/30
    * @Param []
    * @return void
    **/
    private void modifyCustomer(){
        System.out.println("---------------------修改客户---------------------");

        int index;
        Customer customer = null;
        while(true) {
            System.out.print("请选择待修改客户姓名(0退出)：");
            String name = CustomerUtility.readString(4);
            if(name.equals("0")) {
                return;
            }
            index = customerList.searchCustomer(name);
            if(index == -1) {
                System.out.println("该客户不存在！");
                continue;
            }
            customer = customerList.getCustomer(index+1);
            if(customer == null) {
                System.out.println("该客户不存在！");
            } else {
                break;
            }
        }

        System.out.print("姓名(" + customer.getName() + "): ");
        String name = CustomerUtility.readString(4, customer.getName());

        System.out.print("性别(" + customer.getGender() + "): ");
        char gender = CustomerUtility.readChar(customer.getGender());

        System.out.print("年龄(" + customer.getAge() + "): ");
        int age = CustomerUtility.readInt(customer.getAge());

        System.out.print("电话(" + customer.getPhone() + "): ");
        String phone = CustomerUtility.readString(15, customer.getPhone());

        System.out.print("邮箱(" + customer.getEmail() + "): ");
        String email = CustomerUtility.readString(20, customer.getEmail());

        if(customerList.replaceCustomer(index+1, new Customer(name, gender, age, phone, email))) {
            System.out.println("---------------------修改完成---------------------");
        } else {
            System.out.println("----------无法找到指定客户,修改失败--------------");
        }

    }
    /**
    * @Description 通过用户输入的下标 index 进行索引客户对象信息，从而对其进行删除
    * @Date 17:04 2021/8/30
    * @Param []
    * @return void
    **/
    private void deleteCustomer() {
        System.out.println("---------------------删除客户---------------------");

        int index = 0;
        while(true) {
            System.out.print("请选择待删除客户编号(0退出)：");
            index = CustomerUtility.readInt();
            if(index == 0) {
                return;
            }
            if(customerList.getCustomer(index) == null) {
                System.out.println("无法找到该用户！");
            } else {
                break;
            }
        }

        System.out.print("确认是否删除(Y / N)：");
        if(CustomerUtility.readConfirmSelection() == 'N') {
            return;
        }
        if(customerList.deleteCustomer(index)) {
            System.out.println("---------------------删除完成---------------------");
        } else {
            System.out.println("----------无法找到指定客户,删除失败--------------");
        }
    }
    /**
    * @Description 用于遍历已添加的客户信息
    * @Date 17:03 2021/8/30
    * @Param []
    * @return void
    **/
    private void listAllCustomers() {
        System.out.println("---------------------------客户列表---------------------------");
        Customer[] customers = customerList.getCustomers(); //接收客户对象数组
        if(customers.length == 0) {
            System.out.println("没有客户记录！");
        } else {
            System.out.println("序号\t\t姓名\t\t性别\t\t年龄\t\t电话\t\t\t\t\t邮箱");
            for (int i = 0; i < customers.length; i++) {
                System.out.println("No." + (i+1) + "\t" + customers[i].getName() + "\t\t" + customers[i].getGender() + "\t\t" + customers[i].getAge() + "\t\t" + customers[i].getPhone() + "\t\t" + customers[i].getEmail());
            }
        }
        System.out.println("----------------------------------------------------------");
    }
}
