package com.atfunny.BillManagementSystem.javabean;

/**
 * @ClassName Customer
 * @Description 用于封装客户信息
 * @Author AqFunny
 * @Date 2021/8/26 10:36
 * @Version 1.0
 */
public class Customer {
    private String name;    //姓名
    private char gender;    //性别
    private int age;        //年龄
    private String phone;   //电话号码
    private String email;   //电子邮箱

    //无参构造器
    public Customer() {

    }

    //全参构造器
    public Customer(String name, char gender, int age, String phone, String email) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.phone = phone;
        this.email = email;
    }

    //构造器重载
    public Customer(String name, char gender, int age) {
        this(name, gender, age, "null", "null");
    }

    //获得名字
    public String getName() {
        return name;
    }

    //设置名字
    public void setName(String name) {
        this.name = name;
    }

    //获得性别
    public char getGender() {
        return gender;
    }

    //设置性别
    public void setGender(char gender) {
        this.gender = gender;
    }

    //获得年龄
    public int getAge() {
        return age;
    }

    //设置年龄
    public void setAge(int age) {
        this.age = age;
    }

    //获得电话
    public String getPhone() {
        return phone;
    }

    //设置电话
    public void setPhone(String phone) {
        this.phone = phone;
    }

    //获得邮箱
    public String getEmail() {
        return email;
    }

    //设置邮箱
    public void setEmail(String email) {
        this.email = email;
    }
}
