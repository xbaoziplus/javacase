package com.atfunny.BillManagementSystem.service;

import java.util.Scanner;

/**
 * @ClassName CustomerUtility
 * @Description 用于封装本案例的工具类，讲不同的功能封装成静态方法方便直接对其进行调用
 * @Author AqFunny
 * @Date 2021/8/26 10:36
 * @Version 1.0
 */
public class CustomerUtility {
    public static Scanner sc = new Scanner(System.in);

    /**
    * @Description 用于界面菜单的选择，返回类型为整形，返回值为用户输入的选项
    * @Date 12:25 2021/8/29
    * @Param [maxSelection]
    * @return int
    **/
    public static int readMenuSelection(int maxSelection) {
        while(true) {
            int selection = Integer.parseInt(readKeyBoard(1,false));
            if(selection < 1 || selection > maxSelection) {
                System.out.print("输入选项有误，请重新输入：");
                continue;
            }
            return selection;
        }
    }
    /**
    * @Description 读取一个字符，且不允许输入空，并将其作为方法的返回值
    * @Date 16:56 2021/8/29
    * @Param []
    * @return char
    **/
    public static char readChar() {
        return readKeyBoard(1, false).charAt(0);
    }
    /**
    * @Description 读取一个字符，且允许输入空，若直接回车没有输入数据则返回默认值 defaultValue
    * @Date 20:51 2021/8/29
    * @Param [defaultValue]
    * @return char
    **/
    public static char readChar(char defaultValue) {
        String str = readKeyBoard(1, true);
        return str.length() == 0 ? defaultValue : str.charAt(0);
    }
    /**
    * @Description 读取一个不超过二位数的整数，且不允许输入空
    * @Date 16:51 2021/8/29
    * @Param []
    * @return int
    **/
    public static int readInt() {
        int number;
        while(true) {
            try {
                number = Integer.parseInt(readKeyBoard(2, false));
                break;
            } catch (NumberFormatException e) {
                System.out.print("数字输入错误，请重新输入：");
            }
        }
        return number;
    }
    /**
    * @Description 读取一个不超过二位数的整数，且允许输入空，若没有输入数据直接回车则返回 defaultValue
    * @Date 16:39 2021/8/29
    * @Param [defaultValue]指定默认返回值
    * @return int
    **/
    public static int readInt(int defaultValue) {
        int number;
        while(true) {
            String str = readKeyBoard(2, true);
            try {
                number = str.equals("") ? defaultValue : Integer.parseInt(str);
                break;
            } catch (NumberFormatException e) {
                System.out.print("数字输入错误，请重新输入：");
            }
        }
        return number;
    }
    /**
    * @Description 读取一个长度不超过 limit 的字符串，且不允许输入空
    * @Date 20:54 2021/8/29
    * @Param [limit]
    * @return String
    **/
    public static String readString(int limit) {
        return readKeyBoard(limit, false);
    }
    /**
    * @Description 读取一个长度不超过 limit 的字符串，且允许输入空，若没有输入数据直接回车则返回 defaultValue
    * @Date 21:00 2021/8/29
    * @Param [limit, defaultValue]
    * @return java.lang.String
    **/
    public static String readString(int limit, String defaultValue) {
        String str = readKeyBoard(limit, true);
        return str.equals("") ? defaultValue : str;
    }
    /**
    * @Description 用于确认选择的输入，读取‘Y’或‘N’，并将其作为返回值
    * @Date 21:06 2021/8/29
    * @Param []
    * @return char
    **/
    public static char readConfirmSelection() {
        while(true) {
            char ch = readKeyBoard(1, false).toUpperCase().charAt(0);
            if(ch == 'Y' || ch == 'N') {
                return ch;
            } else {
                System.out.print("选择错误，请重新选择：");
            }
        }
    }
    /**
    * @Description 用于记录用户从键盘中输入的数据
    * @Date 12:19 2021/8/29
    * @Param [limit]
    * @return java.lang.String
    **/
    private static String readKeyBoard(int limit, boolean blankReturn) {
        String str = "";
        while(sc.hasNextLine()) {       //若需支持输入空字符即直接回车的话，需要用 hasNextLine
            str = sc.nextLine();
            if(str.length() == 0) {
                if(blankReturn) {
                    return str;
                } else {
                    continue;
                }
            }
            if(str.length() < 1 || str.length() > limit) {
                System.out.print("输入长度错误（不大于" + limit + "），请重新输入：");
                continue;
            }
            break;
        }
        return str;
    }
}
