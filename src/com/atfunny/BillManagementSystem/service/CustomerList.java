package com.atfunny.BillManagementSystem.service;

import com.atfunny.BillManagementSystem.javabean.Customer;

/**
 * @ClassName CustomerList
 * @Description 为Customer对象的管理模块，内部使用数组管理一组Customer对象
 * @Author AqFunny
 * @Date 2021/8/26 10:37
 * @Version 1.0
 */
public class CustomerList {
    private int total;                  //实际用户个数
    private Customer[] customers;       //存放客户对象的数组
    public CustomerList() {
        total = 0;
        customers = new Customer[10];   //默认可存放十个客户对象信息
    }

    /**
    * @author AqFunny
    * @Description 用于初始化 customers 数组
    * @Date 15:14 2021/8/29
    * @Param [totalCustomer]滞定 customers 数组的最大空间
    **/
    public CustomerList(int totalCustomer) {
        customers = new Customer[totalCustomer];
    }
    /**
    * @author AqFunny
    * @Description 将参数 customer 尾插法添加到数组中
    * @Date 15:26 2021/8/29
    * @Param [customer]指定要添加的客户对象
    * @return boolean 添加成功返回 true， 数组已满则为 false
    **/
    public boolean addCustomer(Customer customer) {
        if(customers.length <= this.total) {
            return false;
        }
        customers[this.total++] = customer;
        return true;
    }
    /**
    * @author AqFunny
    * @Description 用参数 customer 替换数组中由 index 指定的对象
    * @Date 15:29 2021/8/29
    * @Param [index, customer] index 指定替换对象在数组中的位置，customer 指定替换的新客户对象
    * @return boolean 替换成功返回 true，索引失败则返回 false
    **/
    public boolean replaceCustomer(int index, Customer customer) {
        if(--index < 0 || index > this.total) {
            return false;
        }
        customers[index] = customer;
        return true;
    }
    /**
    * @Description 通过名字检索指定客户对象
    * @Date 19:53 2021/8/30
    * @Param [name]
    * @return int 若找到返回客户对象所在下标，否则返回-1
    **/
    public int searchCustomer(String name) {
        if(this.total == 0) {
            return -1;
        }
        for (int i = 0; i < this.total; i++) {
            if(customers[i].getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }
    /**
    * @author AqFunny
    * @Description 从数组中删除指定索引位置 index 的客户对象信息
    * @Date 15:37 2021/8/29
    * @Param [index]指定删除对象在数组中的索引位置，用户输入从 1 开始
    * @return boolean
    **/
    public boolean deleteCustomer(int index) {
        if(--index < 0 || index > this.total) {
            return false;
        }
        for (int i = index; i < total-1; i++) {
            customers[i] = customers[i+1];
        }
        customers[--this.total] = null;
        return true;
    }
    /**
    * @author AqFunny
    * @Description 获取数组中记录的所有客户对象
    * @Date 15:43 2021/8/29
    * @Param
    * @return Customer[]
    **/
    public Customer[] getCustomers() {
        Customer[] customersList = new Customer[this.total];
        for (int i = 0; i < this.total; i++) {
            customersList[i] = customers[i];
        }
        return customersList;
    }
    /**
    * @author AqFunny
    * @Description 返回指定索引位置 index 的客户对象信息
    * @Date 15:46 2021/8/29
    * @Param [index]指定所要获取客户对象的索引位置，用户输入从 1 开始
    * @return Customer
    **/
    public Customer getCustomer(int index) {
        if(--index < 0 || index > this.total) {
            return null;
        }
        return customers[index];
    }

    public int getTotal() {
        return total;
    }
}
