package com.atfunny.LibraryMS.javabean;

import com.atfunny.LibraryMS.service.*;
import com.atfunny.LibraryMS.view.LMSUtility;

import java.util.Scanner;

/**
 * @ClassName Admin
 * @Description 管理类员，继承了抽象类 User
 * @Author AqFunny
 * @Date 2021/10/5 20:30
 * @Version 1.0
 */
public class Admin extends User{
    /**
     * @Description 利用构造器，实例化普通用户可以使用的功能
     * @Date 20:22 2021/10/5
     * @Param []
     * @return
     **/
    public Admin(String name) {
        super(name);
        //下面这一代码块不能写在无参构造器中，否则不能执行，导致 operations 为空
        this.operations = new IOOperation[] {
                new ExitSystem(),
                new ListAllBook(),
                new AddBook(),
                new DeleteBook(),
                new ReplaceBook()
        };
    }
    @Override
    public int menu() {
        Scanner sc = new Scanner(System.in);
        System.out.println("=============================");
        System.out.println("Hello " + this.name + ", 欢迎使用图书管理系统!");
        System.out.println("1.查看图书");
        System.out.println("2.增加图书");
        System.out.println("3.删除图书");
        System.out.println("4.修改图书");
        System.out.println("0.退出系统");
        System.out.println("=============================");
        System.out.print("请输入你的选择");
        int selection = new Scanner(System.in).nextInt();
        return selection;
    }
}
