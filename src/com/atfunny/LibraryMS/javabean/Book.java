package com.atfunny.LibraryMS.javabean;

/**
 * @ClassName Book
 * @Description 图书类，用于标记单本书记的基本信息
 * @Author AqFunny
 * @Date 2021/10/4 16:07
 * @Version 1.0
 */
public class Book {
    private String name;        //书名
    private String author;      //作者
    private String publisher;   //出版社
    private String address;     //馆藏位置
    private String status;      //借阅状况
    private String ISBNCode;    //中国大陆标准ISBN码

    public Book() {

    }

    public Book(String name, String author, String publisher, String address, String status) {
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.address = address;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", address='" + address + '\'' +
                ", status='" + status + '\'' +
                ", ISBNCode='" + ISBNCode + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getISBNCode() {
        return ISBNCode;
    }

    public void setISBNCode(String ISBNCode) {
        this.ISBNCode = ISBNCode;
    }
}
