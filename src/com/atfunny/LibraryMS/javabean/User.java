package com.atfunny.LibraryMS.javabean;

import com.atfunny.LibraryMS.service.BookList;
import com.atfunny.LibraryMS.service.IOOperation;

/**
 * @ClassName User
 * @Description 定义用户的抽象类
 * @Author AqFunny
 * @Date 2021/10/5 20:02
 * @Version 1.0
 */
public abstract class User{
    protected String name;                  //用户名
    protected IOOperation[] operations;     //操作对象数组

    public User() {

    }

    public User(String name) {
        this.name = name;
    }
    /**
    * @Description 定义菜单抽象方法，等待该抽象类的子类对其进行重写
    * @Date 20:13 2021/10/5
    * @Param []
    * @return int
    **/
    abstract public int menu();

    /**
    * @Description 通过用户选择，调用对应对象中的 opera 方法
    * @Date 20:11 2021/10/5
    * @Param [selection, bookList]
    * @return void
    **/
    public void doOperation(int selection, BookList bookList) {
        operations[selection].opera(bookList);
    }
}
