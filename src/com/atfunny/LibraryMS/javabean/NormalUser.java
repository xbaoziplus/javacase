package com.atfunny.LibraryMS.javabean;

import com.atfunny.LibraryMS.service.*;
import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName NormalUser
 * @Description 普通用户类，继承了抽象类 User
 * @Author AqFunny
 * @Date 2021/10/5 20:13
 * @Version 1.0
 */
public class NormalUser extends User{
    /**
    * @Description 利用构造器，实例化普通用户可以使用的功能
    * @Date 20:22 2021/10/5
    * @Param []
    * @return
    **/
    public NormalUser(String name) {
        super(name);
        this.operations = new IOOperation[] {
                new ExitSystem(),
                new ListAllBook(),
                new BorrowBook(),
                new ReturnBook()
        };
    }

    @Override
    public int menu() {
        System.out.println("=============================");
        System.out.println("Hello " + this.name + ", 欢迎使用图书管理系统!");
        System.out.println("1.查看图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("=============================");
        System.out.print("请输入你的选择");
        return LMSUtility.readMenuSelection(3);
    }
}
