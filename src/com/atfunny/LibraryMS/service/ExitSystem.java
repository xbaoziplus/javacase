package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName ExitSystem
 * @Description 实现 IOOperation 接口，实现退出系统操作
 * @Author AqFunny
 * @Date 2021/10/5 20:17
 * @Version 1.0
 */
public class ExitSystem implements IOOperation{
    @Override
    public void opera(BookList books) {
        System.out.print("是否确认退出账号(Y/N)");
        char confirm = LMSUtility.readConfirmSelection();
        if(confirm == 'N') {
            return;
        }
        System.out.println("退出成功");
    }
}
