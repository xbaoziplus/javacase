package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.javabean.Book;
import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName AddBook
 * @Description 实现 IOOperation 接口，实现添加书籍功能
 * @Author AqFunny
 * @Date 2021/10/4 22:17
 * @Version 1.0
 */
public class AddBook implements IOOperation{
    @Override
    public void opera(BookList books){
        System.out.print("请输入书籍名称：");
        String name = LMSUtility.readString(30);
        System.out.print("请输入书籍作者：");
        String author = LMSUtility.readString(20);
        System.out.print("请输入书籍出版社：");
        String publisher = LMSUtility.readString(32);
        System.out.print("请输入书籍馆藏地址：");
        String address = LMSUtility.readString(24);

        Book book = new Book(name, author, publisher, address, "free");
        try {
            books.addBook(book);
        } catch (BookException e) {
            System.out.println("出错了！" + e.getMessage());
        }
        System.out.println("添加成功！");
    }
}
