package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName DeleteBook
 * @Description 实现 IOOperation 接口，实现删除书本的操作
 * @Author AqFunny
 * @Date 2021/10/5 11:39
 * @Version 1.0
 */
public class DeleteBook implements IOOperation{
    @Override
    public void opera(BookList books){
        System.out.print("请输入需要删除书籍的ISBN码：");
        String ISBN = LMSUtility.readString(20);
        try {
            books.deleteBook(ISBN);
        } catch (BookException e) {
            System.out.println("出错了！" + e.getMessage());
        }
    }
}
