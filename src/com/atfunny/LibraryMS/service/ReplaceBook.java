package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.javabean.Book;
import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName ReplaceBook
 * @Description 实现 IOOperation 接口，实现更新书籍信息功能
 * @Author AqFunny
 * @Date 2021/10/5 17:28
 * @Version 1.0
 */
public class ReplaceBook implements IOOperation{
    @Override
    public void opera(BookList books) {
        if(books.getBookNumber() == 0) {
            try {
                throw new BookException("馆中目前不存在任何书籍");
            } catch (BookException e) {
                System.out.println("出错了！" + e.getMessage());
            }
        }
        System.out.print("请输入需要进行修改书籍的ISBN码：");
        String ISBN = LMSUtility.readString(20);

        Book[] bookList = books.getBookList();
        for (int i = 0; i < books.getBookNumber(); i++) {
            if(bookList[i].getISBNCode().equals(ISBN)) {
                //修改书籍信息，支持回车跳过，保留原信息不变
                System.out.print("请输入修改后书名(" + bookList[i].getName() + ")：");
                String name = LMSUtility.readString(30,bookList[i].getName());
                System.out.print("请输入修改后作者(" + bookList[i].getAuthor() + ")：");
                String author = LMSUtility.readString(20,bookList[i].getAuthor());
                System.out.print("请输入修改后出版社(" + bookList[i].getPublisher() + ")：");
                String publisher = LMSUtility.readString(32,bookList[i].getPublisher());
                System.out.print("请输入修改后馆藏地址(" + bookList[i].getAddress() + ")：");
                String address = LMSUtility.readString(24,bookList[i].getAddress());

                Book modifiedBook = new Book(name, author, publisher, address, bookList[i].getStatus());    //新建书籍对象
                modifiedBook.setISBNCode(bookList[i].getISBNCode());    //将 ISBN 码直接赋值过去
                bookList[i] = modifiedBook;     //将新建的书籍对象覆盖原对象
                System.out.println("修改成功");
                return;
            }
        }
        try {
            throw new BookException("ISBN码为 \"" + ISBN + "\" 的书籍不存在，无法修改");
        } catch (BookException e) {
            System.out.println("出错了！" + e.getMessage());
        }
    }
}
