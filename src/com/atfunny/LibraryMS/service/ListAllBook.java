package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.javabean.Book;

/**
 * @ClassName ListAllBook
 * @Description 实现 IOOperation 接口，展现所有的书籍信息
 * @Author AqFunny
 * @Date 2021/10/5 11:46
 * @Version 1.0
 */
public class ListAllBook implements IOOperation{
    @Override
    public void opera(BookList books){
        Book[] allBook = books.getBookList();
        System.out.println("\n============================书籍信息============================\n");
        for (int i = 0; i < books.getBookNumber(); i++) {
            System.out.println(allBook[i].toString());
        }
        System.out.println("\n===============================================================\n");
    }
}
