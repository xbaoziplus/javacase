package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.javabean.Book;

/**
 * @InterfaceName IOOperation
 * @Description 定义一个对书籍操作的接口
 * @Author AqFunny
 * @Date 2021/10/4 22:18
 * @Version 1.0
 */
public interface IOOperation{
    public void opera(BookList books);
}
