package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.javabean.Book;
import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName BorrowBook
 * @Description 实现 IOOperation 接口，实现借书操作
 * @Author AqFunny
 * @Date 2021/10/5 18:03
 * @Version 1.0
 */
public class BorrowBook implements IOOperation{
    @Override
    public void opera(BookList books) {
        if(books.getBookNumber() == 0) {
            try {
                throw new BookException("馆中目前不存在任何书籍");
            } catch (BookException e) {
                System.out.println("出错了！" + e.getMessage());
            }
        }
        System.out.print("请输入需要进行租借书籍的ISBN码：");
        String ISBN = LMSUtility.readString(20);
        //遍历搜索书籍
        Book[] bookList = books.getBookList();
        for (int i = 0; i < books.getBookNumber(); i++) {
            if (bookList[i].getISBNCode().equals(ISBN)) {
                //书籍已被借出
                if(bookList[i].getStatus().equals("borrowed")) {
                    try {
                        throw new BookException("很抱歉，书籍已借出");
                    } catch (BookException e) {
                        System.out.println(e.getMessage());
                    }
                    return;
                }
                //设立确认环节
                System.out.print("是否确认租借《" + bookList[i].getName() + "》(Y/N)");
                char confirm = LMSUtility.readConfirmSelection();
                if(confirm == 'N') {
                    return;
                }
                bookList[i].setStatus("borrowed");
                System.out.println("租借成功");
                return;
            }
        }
        //书籍不存在
        try {
            throw new BookException("ISBN码为 \"" + ISBN + "\" 的书籍不存在，无法租借");
        } catch (BookException e) {
            System.out.println("出错了！" + e.getMessage());
        }
    }
}
