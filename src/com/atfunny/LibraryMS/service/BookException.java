package com.atfunny.LibraryMS.service;

/**
 * @ClassName BookException
 * @Description 书本操作时可能出现的错误
 * @Author AqFunny
 * @Date 2021/10/4 17:00
 * @Version 1.0
 */
public class BookException extends Exception{
    static final long serialVersionUID = -7034816545125766939L;

    public BookException() {

    }

    public BookException(String message) {
        super(message);
    }
}
