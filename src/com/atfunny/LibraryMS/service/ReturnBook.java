package com.atfunny.LibraryMS.service;

import com.atfunny.LibraryMS.javabean.Book;
import com.atfunny.LibraryMS.view.LMSUtility;

/**
 * @ClassName ReturnBook
 * @Description 实现 IOOperation，实现归还书籍操作
 * @Author AqFunny
 * @Date 2021/10/5 19:41
 * @Version 1.0
 */
public class ReturnBook implements IOOperation{
    @Override
    public void opera(BookList books) {
        System.out.print("请输入要归还书籍的ISBN码：");
        String ISBN = LMSUtility.readString(20);
        //遍历书籍
        Book[] bookList = books.getBookList();
        for (int i = 0; i < books.getBookNumber(); i++) {
            if (bookList[i].getISBNCode().equals(ISBN)) {
                //书籍仍在馆中
                if(bookList[i].getStatus().equals("free")) {
                    try {
                        throw new BookException("这本书籍未被借阅，无需归还");
                    } catch (BookException e) {
                        System.out.println("出错了！" + e.getMessage());
                    }
                }
                //设立确认环节
                System.out.print("是否确认归还《" + bookList[i].getName() + "》(Y/N)");
                char confirm = LMSUtility.readConfirmSelection();
                if(confirm == 'N') {
                    return;
                }
                bookList[i].setStatus("free");
                System.out.println("归还成功");
                return;
            }
        }
        //书籍不存在，抛出异常
        try {
            throw new BookException("ISBN码为 \"" + ISBN + "\" 的书籍不存在，无法归还");
        } catch (BookException e) {
            System.out.println("出错了！" + e.getMessage());
        }
    }
}
