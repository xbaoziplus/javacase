package com.atfunny.LibraryMS.view;

import com.atfunny.LibraryMS.javabean.Admin;
import com.atfunny.LibraryMS.javabean.NormalUser;
import com.atfunny.LibraryMS.javabean.User;
import com.atfunny.LibraryMS.service.BookList;

/**
 * @ClassName MainText
 * @Description 主测试类
 * @Author AqFunny
 * @Date 2021/10/5 20:40
 * @Version 1.0
 */
public class MainText {
    public static void main(String[] args) {
        BookList bookList = new BookList();
        User user = login();
        while(true) {
            //判断是否退出程序
            if(user == null) {
                break;
            }
            int selection = user.menu();
            //对退出账号进行处理
            if(selection == 0) {
                user = login();
                continue;
            }
            user.doOperation(selection, bookList);
        }
    }
    /**
    * @Description 用于用户登陆
    * @Date 16:08 2021/10/6
    * @Param []
    * @return com.atfunny.LibraryMS.javabean.User
    **/
    public static User login() {
        System.out.println("1. 普通用户\t\t2. 管理员");
        System.out.println("0. 退出系统");
        System.out.print("请进行您的选择：");
        int selection = LMSUtility.readMenuSelection(2);
        if(selection == 0) {
            return null;
        }
        System.out.print("请输入您的姓名：");
        String name = LMSUtility.readString(10);
        return selection == 1 ? new NormalUser(name) : new Admin(name);
    }
}
