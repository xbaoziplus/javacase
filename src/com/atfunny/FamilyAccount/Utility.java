package com.atfunny.FamilyAccount;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author AqFunny
 * @version 1.0
 * Utility 工具类封装了 FamilyAccount 所需的方法
 */
public class Utility {
    private static Scanner sc = new Scanner(System.in);

    /**
     * 用于界面菜单的选择。
     * 如果接收到合法选项则返回。返回值为用户所输入的选项
     * @param maxSelection
     * @return
     */
    public static int readMenuSelection(int maxSelection) {
        int selection;
        String str;
        while(true) {
            str = readKeyBoard(1);
            selection = Integer.parseInt(str);
            if(selection < 0 || selection > maxSelection) {
                System.out.println("所输入的选项不存在");
                System.out.print("请重新输入：");
                continue;
            }
            break;
        }
        return selection;
    }

    /**
     * 用于用户输入限定长度为6的开销金额。
     * 返回值为开销金额，返回类型为double
     * @param type
     * @return incomeAccount
     */
    public static double readExpenseAccount(String type) {
        double incomeAccount;
        while(true) {
            String str = readKeyBoard(6);
            try {
                incomeAccount = Double.parseDouble(str);
                if(type.equals("收入")) {
                    return incomeAccount;
                } else {
                    return incomeAccount * -1;
                }
            } catch (NumberFormatException e) {
                System.out.println("输入金额错误");
                System.out.print("请重新输入：");
            }
        }
    }

    /**
     * 用于用户输入长度不多于8的开销描述。
     * 返回值为该描述，返回类型为String
     * @return
     */
    public static String readExpenseDescription() {
        return readKeyBoard(8);
    }

    /**
     * 用于通过开销描述查找目标账单。
     * 返回类型为int
     * 若查询成功返回所处下标，否则返回-1
     * @param accountInfoList
     * @return
     */
    public static int queryAccountInfo(ArrayList<AccountInfo> accountInfoList) {
        String str = readExpenseDescription();
        for (int i = 0; i < accountInfoList.size(); i++) {
            if(accountInfoList.get(i).getExpenseDescription().equals(str)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 用于退出界面的确定选择。
     * 如果接收到合法选项则返回。返回值为用户所输入的选项
     * @return
     */
    public static boolean readConfirmSelection() {
        String exitInfo;
        while(true) {
            exitInfo = readKeyBoard(1).toUpperCase();   //统一转换成大写
            if(exitInfo.equals("Y")) {
                return true;
            } else if(exitInfo.equals("N")) {
                return false;
            }
            System.out.println("输入选项错误");
            System.out.print("请重新输入：");
        }
    }

    /**
     * 用于接收键盘输入的不大于限制长度为 limit 的字符串。
     * 返回值为该字符串
     * @param limit
     * @return
     */
    public static String readKeyBoard(int limit) {
        String str = "";
        while(sc.hasNext()) {
            str = sc.nextLine();
            if(str.length() > limit) {
                System.out.println("输入长度出错（不超过" + limit + "）");
                System.out.print("请重新输入：");
                continue;
            }
            break;
        }
        return str;
    }
}
