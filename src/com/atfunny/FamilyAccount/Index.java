package com.atfunny.FamilyAccount;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Index {
    private static final int MAXSELECTION = 6;
    private static double totalCost;

    public Index() {
        this.totalCost = 0;
    }

    public static void main(String[] args) {
        boolean isFlag = true;  //判断是否结束程序
        boolean isExit;         //判断是否确定退出
        int selection;          //功能选项
        Date date = new Date(); //实例化Date类
        ArrayList<AccountInfo> accountInfoList = new ArrayList<AccountInfo>();  //创建一个数据类型为 AccountInfo 的 ArrayList
        //菜单界面
        while(isFlag) {
            System.out.println("\n---------------------家庭收支记账软件---------------------\n");
            System.out.println("\t1、收支明细\t\t\t\t\t\t2、登记收入");
            System.out.println("\t3、登记支出\t\t\t\t\t\t4、查询账单");
            System.out.println("\t5、删除账单\t\t\t\t\t\t6、退   出\n");
            System.out.print("\t\t\t\t\t请选择（1-" + MAXSELECTION + "）：");
            selection = Utility.readMenuSelection(MAXSELECTION);
            switch (selection) {
                //收支明细
                case 1: {
                    System.out.println("收支明细");
                    System.out.println("-----------------------------------------------------");
                    System.out.println("收支\t\t收支金额\t\t收支说明\t\t开销时间");
                    System.out.println("-----------------------------------------------------");
                    for (int i = 0; i < accountInfoList.size(); i++) {
                        System.out.println(accountInfoList.get(i).getExpensesType() + "\t\t￥" + accountInfoList.get(i).getExpensesAmount() + "\t\t" + accountInfoList.get(i).getExpenseDescription() + "\t\t" + accountInfoList.get(i).getExpenseDate());
                    }
                    System.out.println("-----------------------------------------------------");
                    System.out.println("总开销为：￥" + totalCost);
                    break;
                }
                //登记收入
                case 2: {
                    System.out.println("登记收入");
                    AccountInfo accountInfo = new AccountInfo("收入");
                    //收入金额
                    System.out.print("请输入收入金额：");
                    accountInfo.setExpensesAmount(Utility.readExpenseAccount("收入"));
                    totalCost += accountInfo.getExpensesAmount();
                    //收入描述
                    System.out.print("请输入收入描述：");
                    accountInfo.setExpenseDescription(Utility.readExpenseDescription());
                    accountInfo.setExpenseDate(new SimpleDateFormat("yyyy年MM月dd日 HH:mm").format(date));
                    //插入ArrayList
                    accountInfoList.add(0, accountInfo);
                    System.out.println("登记成功！");
                    break;
                }
                //登记支出
                case 3: {
                    System.out.println("登记支出");
                    AccountInfo accountInfo = new AccountInfo("支出");
                    //支出金额
                    System.out.print("请输入支出金额：");
                    accountInfo.setExpensesAmount(Utility.readExpenseAccount("支出"));
                    totalCost += accountInfo.getExpensesAmount();
                    //支出描述
                    System.out.print("请输入支出描述：");
                    accountInfo.setExpenseDescription(Utility.readExpenseDescription());
                    accountInfo.setExpenseDate(new SimpleDateFormat("yyyy年MM月dd日 HH:mm").format(date));
                    //插入ArrayList
                    accountInfoList.add(0, accountInfo);
                    System.out.println("登记成功！");
                    break;
                }
                //查询账单
                case 4: {
                    System.out.println("查询账单");
                    if(accountInfoList.size() == 0) {
                        System.out.println("没有检测到数据");
                        System.out.println("先登记账单哦");
                        break;
                    }
                    System.out.print("请输入需要查到的收支说明：");
                    int index = Utility.queryAccountInfo(accountInfoList);
                    if(index == -1) {
                        System.out.println("查询失败");
                        System.out.println("请检查是否输入有误");
                    } else{
                        System.out.println("收支\t\t收支金额\t\t收支说明\t\t开销时间");
                        System.out.println("-----------------------------------------------------");
                        System.out.println(accountInfoList.get(index).getExpensesType() + "\t\t￥" + accountInfoList.get(index).getExpensesAmount() + "\t\t" + accountInfoList.get(index).getExpenseDescription() + "\t\t" + accountInfoList.get(index).getExpenseDate());
                    }
                    break;
                }
                //删除账单
                case 5: {
                    System.out.println("删除账单");
                    if(accountInfoList.size() == 0) {
                        System.out.println("没有检测到数据");
                        System.out.println("先登记账单哦");
                        break;
                    }
                    System.out.print("请输入需要删除的收支说明：");
                    int index = Utility.queryAccountInfo(accountInfoList);
                    if(index == -1) {
                        System.out.println("删除失败");
                        System.out.println("请检查是否输入有误");
                    } else{
                        accountInfoList.remove(index);
                        System.out.println("删除成功");
                    }
                    break;
                }
                //退出
                case 6: {
                    System.out.println("退出");
                    System.out.print("是否确定退出(Y/N): ");
                    isExit = Utility.readConfirmSelection();
                    if(isExit) {
                        isFlag = false;
                        System.out.println("退出成功");
                    }
                    break;
                }
            }
        }

    }
}
