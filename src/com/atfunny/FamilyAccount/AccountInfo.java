package com.atfunny.FamilyAccount;

/**
 * 开销信息结构
 */
public class AccountInfo {
    public AccountInfo() {
        this.expensesAmount = 0;
        this.expenseDescription = "无";
    }
    public AccountInfo(String expensesType) {
        this.expensesType = expensesType;
    }

    private String expensesType;        //开销种类
    private double expensesAmount;      //开销金额
    private String expenseDescription;  //开销说明
    private String expenseDate;         //开销时间
    /*获取开销种类*/
    public String getExpensesType() {
        return expensesType;
    }
    /*获取开销金额*/
    public double getExpensesAmount() {
        return expensesAmount;
    }
    /*获取开销说明*/
    public String getExpenseDescription() {
        return expenseDescription;
    }
    /*获取开销时间*/
    public String getExpenseDate() {
        return expenseDate;
    }

    /*修改开销种类*/
    public void setExpensesType(String expensesType) {
        this.expensesType = expensesType;
    }
    /*修改开销金额*/
    public void setExpensesAmount(double expensesAmount) {
        this.expensesAmount = expensesAmount;
    }
    /*修改开销说明*/
    public void setExpenseDescription(String expenseDescription) {
        this.expenseDescription = expenseDescription;
    }
    /*修改开销时间*/
    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }
}
